/**
 * @template T
 */
export class ObjectStorage {

    /** @type {Object<string, T>} */
    storage = {};

    /**
     * @param {string} id
     */
    has(id) {
        return this.storage[id] !== undefined;
    }

    /**
     * @returns {T[]}
     */
    all() {
        return Object.values(this.storage);
    }

    /**
     * @param {string} id
     * @param {T} object
     */
    add(id, object) {
        if (this.has(id)) {
            throw `Object with id ${id} already exists`;
        }
        this.storage[id] = object;
    }

    /**
     * @param {string} id
     */
    remove(id) {
        delete this.storage[id];
    }
}
