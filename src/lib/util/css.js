/**
 * @typedef {('none'|'int')} GetCssVariableOptParse
 */

/**
 * @typedef GetCssVariableOpt
 * @property {Element=} element
 * @property {GetCssVariableOptParse=} parse
 */

/**
 * @type {Object.<string, Object.<GetCssVariableOptParse, string|number>>}
 */
const staticCssVariables = {};

/**
 * Gets the value of a css variable. The base element in `options.element` (default is
 * `document.body`). If `options.parse` is given, the value will be parsed/converted to the given
 * type (only available for 'int').
 *
 * @param {string} name
 * @param {GetCssVariableOpt=} options
 * @returns {string|number}
 */
export function getCssVariable(name, options = {}) {
    /** @type {string|number} */
    let value = getComputedStyle(options.element || document.body).getPropertyValue(name);

    if (options.parse === 'int') {
        value = parseInt(value, 10);
        if (isNaN(value)) {
            value = 0;
        }
    }

    return value;
}

/**
 * Gets a root css variable by assuming it will never change (value is stored statically).
 *
 * @param {string} name
 * @param {GetCssVariableOpt=} options
 * @returns {string|number}
 */
export function getStaticCssVariable(name, options = {}) {
    staticCssVariables[name] ||= {};
    /** @type {GetCssVariableOptParse} */
    const parse = options.parse || 'none';

    if (!(parse in staticCssVariables[name])) {
        options = { ...options, ...{ element: undefined } };
        staticCssVariables[name][parse] = getCssVariable(name, options);
    }

    return staticCssVariables[name][parse];
}

/**
 * Gets a root css variable by assuming it will never change (value is stored statically).
 * The value is treated as a string.
 *
 * @param {string} name
 * @param {GetCssVariableOpt=} options
 * @returns {string}
 */
export function getStaticStrCssVariable(name, options = {}) {
    options = { ...options, ...{ parse: 'none' } };
    return /** @type {string} */ (getStaticCssVariable(name, options));
}

/**
 * Gets a root css variable by assuming it will never change (value is stored statically).
 * The value is treated as a number.
 *
 * @param {string} name
 * @param {GetCssVariableOpt=} options
 * @returns {number}
 */
export function getStaticIntCssVariable(name, options = {}) {
    options = { ...options, ...{ parse: 'int' } };
    return /**@type {number} */ (getStaticCssVariable(name, options));
}

/**
 * @param {string} name
 * @param {string|number} value
 */
export function setCssVariable(name, value) {
    document.documentElement.style.setProperty(name, `${value}`);
}
