/** @typedef {import('svelte/store').Unsubscriber} Unsubscriber */
/** @template T @typedef {import('svelte/store').Subscriber<T>} Subscriber */
/** @template T @typedef {import('svelte/store').Readable<T>} Readable */

export class SubscribeHandler {
    /** @type {Unsubscriber[]} */
    unsubscribers = [];

    /**
     * @template T
     * @param {Readable<T>} store
     * @param {Subscriber<T>} subscriber
     */
    subscribe(store, subscriber) {
        this.unsubscribers.push(store.subscribe(subscriber));
    }

    unsubscribeAll() {
        for (const unsubscribe of this.unsubscribers) {
            unsubscribe();
        }
        this.unsubscribers = [];
    }
}
