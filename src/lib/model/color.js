import { hexToRgb, hslToRgb, rgbToHex, rgbToHsl } from '$lib/util/color';

/**
 * @typedef {import('$lib/util/color').Rgb} Rgb
 */

/**
 * @typedef {import('$lib/util/color').Hsl} Hsl
 */

export class Color {
    /**
     * @param {number} r
     * @param {number} g
     * @param {number} b
     * @param {number} h
     * @param {number} s
     * @param {number} l
     * @param {string} hex
     */
    constructor(r, g, b, h, s, l, hex) {
        this._r = r;
        this._g = g;
        this._b = b;
        this._h = h;
        this._s = s;
        this._l = l;
        this._hex = hex;
    }

    /**
     * @param {Rgb} rgb
     * @returns Color
     */
    static fromRgb({r, g, b}) {
        const {h, s, l} = rgbToHsl({r, g, b});
        const hex = rgbToHex({r, g, b});

        return new Color(r, g, b, h, s, l, hex);
    }

    /**
     * @param {Hsl} hsl
     * @returns Color
     */
    static fromHsl({h, s, l}) {
        const {r, g, b} = hslToRgb({h, s, l});
        const hex = rgbToHex({r, g, b});

        return new Color(r, g, b, h, s, l, hex);
    }

    /**
     * @param {string} hex
     * @returns {Color|null}
     */
    static fromHex(hex) {
        const rgb = hexToRgb(hex);
        if (rgb === null) {
            return null;
        }
        const {r, g, b} = rgb;
        const {h, s, l} = rgbToHsl({r, g, b});

        return new Color(r, g, b, h, s, l, hex);
    }

    /**
     * @param {string} hex
     * @returns {Color}
     */
    static fromHexFallbackBlack(hex) {
        const rgb = hexToRgb(hex) || {r: 0, g: 0, b: 0};
        const {r, g, b} = rgb;
        const {h, s, l} = rgbToHsl({r, g, b});

        return new Color(r, g, b, h, s, l, hex);
    }

    /**
     * @returns {Color}
     */
    static default() {
        return this.black();
    }

    /**
     * @returns {Color}
     */
    static black() {
        return new Color(0, 0, 0, 0, 0, 0, '#000000');
    }

    /**
     * @returns {Color}
     */
    static white() {
        return new Color(255, 255, 255, 0, 0, 1, '#FFFFFF');
    }

    clone() {
        return new Color(
            this._r,
            this._g,
            this._b,
            this._h,
            this._s,
            this._l,
            this._hex,
        );
    }

    get r() {
        return this._r;
    }

    get g() {
        return this._g;
    }

    get b() {
        return this._b;
    }

    get h() {
        return this._h;
    }

    get s() {
        return this._s;
    }

    get l() {
        return this._l;
    }

    get hex() {
        return this._hex;
    }
}
