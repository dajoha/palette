import { writable } from 'svelte/store';

/** @template T @typedef {import('svelte/store').Writable<T>} Writable */

/**
 * @typedef {Object.<string, true>} PopupState
 */

/**
 * @typedef {{
 *     nbOpenedPopups: Writable<number>,
 *     openPopup: function(string): void,
 *     closePopup: function(string): void,
 * }} PopupStateStoreExtra
 */

function createPopupState() {
    /** @type {PopupState} */
    const openedPopups = {};

    const { subscribe, set, update } = writable(openedPopups);
    const nbOpenedPopups = writable(0);

    return {
        subscribe,
        set,
        update,
        nbOpenedPopups,
        /**
         * @param {string} id
         */
        openPopup: (id) => update(openedPopups => {
            if (!(id in openedPopups)) {
                openedPopups[id] = true;
                nbOpenedPopups.set(Object.keys(openedPopups).length);
            }
            return openedPopups;
        }),
        /**
         * @param {string} id
         */
        closePopup: (id) => update(openedPopups => {
            if (id in openedPopups) {
                delete openedPopups[id];
                nbOpenedPopups.set(Object.keys(openedPopups).length);
            }
            return openedPopups;
        }),
    };
}

/**
 * @type {Writable<PopupState> & PopupStateStoreExtra}
 */
export const popupState = createPopupState();
