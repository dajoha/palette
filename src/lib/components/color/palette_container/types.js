/** @typedef {import('$lib/components/color/palette/context').PaletteContext} PaletteContext */

/**
 * @typedef {{
 *     id: string,
 *     context: PaletteContext,
 *     maxColumns?: number,
 * }} PaletteItem
 */

/**
 * @typedef {{
 *     context: PaletteContext,
 *     maxColumns?: number,
 * }} AddPaletteOptions
 */

/**
 * @typedef {{
 *     context?: PaletteContext,
 *     maxColumns?: number,
 * }} AddCollectionOptions
 */

export default {};
