import { Color } from '$lib/model/color';
import { uid } from 'uid';
import { ColorBoxContext } from '$lib/components/color/color_box';
import { get, writable } from 'svelte/store';

/** @template T @typedef {import('svelte/store').Writable<T>} Writable */
/** @typedef {import('../../types').ColorBoxItem} ColorBoxItem */

/**
 * @typedef {{
 *     initialColors?: Color[],
 * }} ConstructorOptions
 */

/**
 * @typedef {{
 *     initialColor?: Color,
 * }} AddOptions
 */

export class CollectionTypeContext {
    /** @type {Writable<ColorBoxItem[]>} */
    colorBoxItems = writable([]);

    /**
     * @param {ConstructorOptions=} options
     */
    constructor(options = {}) {
        const initialColors = options.initialColors || [];
        for (const initialColor of initialColors) {
            this.add({
                initialColor,
            });
        }
    }

    leave() {
        for (const colorBoxItem of get(this.colorBoxItems)) {
            const colorBoxContext = colorBoxItem.props.context;
            if (typeof colorBoxContext?.leave === 'function') {
                colorBoxContext.leave();
            }
        }
    }

    /**
     * @param {AddOptions} options
     * @returns {ColorBoxItem}
     */
    add(options = {}) {
        const context = new ColorBoxContext({
            initialColor: options.initialColor || Color.default(),
        });

        /** @type {ColorBoxItem} */
        const colorBoxItem = {
            id: uid(),
            props: {
                context,
            },
        };

        this.colorBoxItems.update(colorBoxItems => {
            colorBoxItems.push(colorBoxItem);
            return colorBoxItems;
        });

        return colorBoxItem;
    }

    /**
     * @param {string} id
     * @param {?ColorBoxContext=} colorBoxContext
     */
    remove(id, colorBoxContext) {
        const colorBoxItems = get(this.colorBoxItems);
        const found = colorBoxItems.findIndex(item => item.id === id);
        if (found === -1) {
            // Should never happen
            return;
        }

        // Not mandatory, but it will perform a smoother transition:
        colorBoxContext?.stopEdit();

        // TODO: check if `setTimeout` is still useful:
        // setTimeout: avoid the popup to disappear only after the remove
        // transition has finished:
        setTimeout(() => {
            this.colorBoxItems.update(colorBoxItems => {
                colorBoxContext?.leave();
                colorBoxItems.splice(found, 1);
                return colorBoxItems;
            });
        }, 0);
    }
}
