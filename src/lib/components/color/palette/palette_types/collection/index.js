import Content from './Content.svelte';
import { CollectionTypeContext } from './type_context';

/** @typedef {import('../types').PaletteTypeDefinition} PaletteTypeDefinition */

/** @type {PaletteTypeDefinition} */
export default {
    content: Content,
    typeContextClass: CollectionTypeContext,
    label: 'Collection',
};
