import Content from './Content.svelte';
import { GradientTypeContext } from './type_context';

/** @typedef {import('../types').PaletteTypeDefinition} PaletteTypeDefinition */

/** @type {PaletteTypeDefinition} */
export default {
    content: Content,
    typeContextClass: GradientTypeContext,
    label: 'Gradient',
};
