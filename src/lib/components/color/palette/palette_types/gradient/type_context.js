import { Color } from '$lib/model/color';
import { uid } from 'uid';
import { ColorBoxContext } from '$lib/components/color/color_box';
import { get, writable } from 'svelte/store';
import { SubscribeHandler } from '$lib/util/subscribe_handler';

/** @template T @typedef {import('svelte/store').Writable<T>} Writable */
/** @template T @typedef {import('svelte/store').Subscriber<T>} Subscriber */
/** @typedef {import('svelte/store').Unsubscriber} Unsubscriber */
/** @template T @typedef {import('svelte/store').Readable<T>} Readable */
/** @typedef {import('../../types').ColorBoxItem} ColorBoxItem */

/**
 * @typedef {{
 *     startColor?: Color,
 *     endColor?: Color,
 * }} ConstructorOptions
 */

/**
 * TODO: move this method near `ColorBoxItem`.
 * @param {{
 *     initialColor?: Color,
 * }} options
 * @returns {ColorBoxItem}
 */
function createColorBoxItem(options = {}) {
    const context = new ColorBoxContext({
        initialColor: options.initialColor || Color.default(),
    });

    /** @type {ColorBoxItem} */
    return {
        id: uid(),
        props: {
            context,
        },
    };
}

export class GradientTypeContext {
    /** @type {ColorBoxItem} */
    startColorBoxItem;

    /** @type {ColorBoxItem} */
    endColorBoxItem;

    /** @type {Writable<number>} */
    nbSteps;

    /** @type {Writable<ColorBoxItem[]>} */
    stepColorBoxItems = writable([]);

    /**
     * @param {ConstructorOptions=} options
     */
    constructor(options = {}) {
        const initialNbSteps = 5;

        this.startColorBoxItem = createColorBoxItem({ initialColor: options.startColor });
        this.endColorBoxItem = createColorBoxItem({ initialColor: options.endColor });
        this.nbSteps = writable(initialNbSteps);

        this.subscribeHandler = new SubscribeHandler();

        /** @type {Writable<Color>} */
        this.startColor;
        /** @type {Unsubscriber=} */
        this.startColorUnsubscribe;
        this.subscribe(this.startColorBoxItem.props.context.colorStore, (cs) => {
            this.startColor = cs;
            if (this.startColorUnsubscribe) {
                this.startColorUnsubscribe();
                this.startColorUnsubscribe = this.startColor.subscribe(() => {
                    this.updateStepColors(false);
                });
            }
        });

        /** @type {Writable<Color>} */
        this.endColor;
        /** @type {Unsubscriber=} */
        this.endColorUnsubscribe;
        this.subscribe(this.endColorBoxItem.props.context.colorStore, (cs) => {
            this.endColor = cs;
            if (this.endColorUnsubscribe) {
                this.endColorUnsubscribe();
                this.endColorUnsubscribe = this.endColor.subscribe(() => {
                    this.updateStepColors(false);
                });
            }
        });

        this.subscribe(this.nbSteps, () => {
            this.updateStepColors(true);
        });
        this.startColorUnsubscribe = this.startColor.subscribe(() => {
            this.updateStepColors(false);
        });
        this.endColorUnsubscribe = this.endColor.subscribe(() => {
            this.updateStepColors(false);
        });
    }

    /**
     * @param {boolean} recreate
     */
    updateStepColors(recreate) {
        const nbSteps = get(this.nbSteps);
        const startColor = get(this.startColor);
        const endColor = get(this.endColor);

        if (nbSteps < 2) {
            return;
        }

        if (recreate) {
            const stepColorBoxItems = [];
            for (let i = 0; i !== nbSteps; i++) {
                stepColorBoxItems.push(createColorBoxItem());
            }
            this.leaveStepColorBoxItems();
            this.stepColorBoxItems.set(stepColorBoxItems);
        }

        // TODO: update() may not be necessary
        this.stepColorBoxItems.update((stepColorBoxItems) => {
            const [r1, g1, b1] = [startColor.r, startColor.g, startColor.b];
            const [r2, g2, b2] = [endColor.r, endColor.g, endColor.b];
            const [dr, dg, db] = [r2 - r1, g2 - g1, b2 - b1];

            for (let i = 0; i !== nbSteps; i++) {
                const r = r1 + i * dr / (nbSteps - 1);
                const g = g1 + i * dg / (nbSteps - 1);
                const b = b1 + i * db / (nbSteps - 1);
                const newColor = Color.fromRgb({r, g, b});
                const color = get(stepColorBoxItems[i].props.context.colorStore);
                color.set(newColor);
            }

            return stepColorBoxItems;
        });
    }

    /**
     * @template T
     * @param {Readable<T>} store
     * @param {Subscriber<T>} subscriber
     */
    subscribe(store, subscriber) {
        this.subscribeHandler.subscribe(store, subscriber);
    }

    leave() {
        this.subscribeHandler.unsubscribeAll();
        this.startColorUnsubscribe && this.startColorUnsubscribe();
        this.endColorUnsubscribe && this.endColorUnsubscribe();

        this.startColorBoxItem.props.context?.leave();
        this.endColorBoxItem.props.context?.leave();
        this.leaveStepColorBoxItems();
    }

    leaveStepColorBoxItems() {
        for (const boxItem of get(this.stepColorBoxItems)) {
            boxItem.props.context?.leave();
        }
    }
}
