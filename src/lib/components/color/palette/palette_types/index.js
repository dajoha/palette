import collection from './collection';
import gradient from './gradient';

/** @typedef {import('./types').PaletteType} PaletteType */
/** @typedef {import('./types').PaletteTypeDefinition} PaletteTypeDefinition */

/** @type {Object.<PaletteType, PaletteTypeDefinition>} */
export const paletteTypes = {
    collection,
    gradient,
};
