/** @typedef {import('svelte').ComponentType} ComponentType */

/**
 * @typedef {'collection'|'gradient'} PaletteType
 */

/**
 * @typedef {object & {
 *     leave?: function(): void,
 *     enter?: function(): void,
 * }} TypeContext
 */

/**
 * @typedef {{
 *     content: ComponentType,
 *     typeContextClass: (new() => TypeContext),
 *     label: string,
 * }} PaletteTypeDefinition
 */

export default {};
