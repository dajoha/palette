/** @typedef {import('$lib/components/color/color_box/context').ColorBoxContext} ColorBoxContext */
/** @typedef {import('$types').Color} Color */

/**
 * @typedef {{
 *     context: ColorBoxContext,
 *     initialColor?: Color,
 * }} ColorBoxProps
 */

/**
 * @typedef {{
 *     id: string,
 *     props: ColorBoxProps,
 * }} ColorBoxItem
 */

export default {};
