import { getContext, setContext } from 'svelte';
import { get, writable } from 'svelte/store';
import { SubscribeHandler } from '$lib/util/subscribe_handler';
import { paletteTypes } from './palette_types';
import { paletteStorage } from '$lib/stores/palette_storage';

// TODO: to remove
let nbPalettes = 0;

/** @template T @typedef {import('svelte/store').Writable<T>} Writable */
/** @typedef {import('./palette_types/types').PaletteType} PaletteType */
/** @typedef {import('./palette_types/types').TypeContext} TypeContext */
/** @typedef {import('./palette_types/types').PaletteTypeDefinition} PaletteTypeDefinition */

export const rootContextKey = Symbol();

/**
 * @param {PaletteType} type
 * @returns {TypeContext}
 */
function createTypeContext(type) {
    /** @type {PaletteTypeDefinition} */
    const def = paletteTypes[type];
    return new def.typeContextClass();
}

/**
 * @typedef {{
 *     initial?: {
 *         type: PaletteType,
 *         typeContext: TypeContext,
 *     },
 * }} ConstructorOptions
 */

export class PaletteContext {
    /** @type {Writable<string>} */
    name;

    /** @type {Writable<PaletteType>} */
    type;

    /** @type {Writable<TypeContext>} */
    typeContext;

    /** @type {SubscribeHandler} */
    subscribeHandler;

    /**
     * @param {ConstructorOptions=} opts
     */
    constructor(opts = {}) {
        this.name = writable(`Palette ${++nbPalettes}`);

        const initialType = opts.initial?.type || 'collection';
        this.type = writable(initialType);

        this.typeContext = writable(opts.initial?.typeContext || createTypeContext(initialType));

        this.subscribeHandler = new SubscribeHandler();
        let first = true;
        this.subscribeHandler.subscribe(this.type, (type) => {
            if (first) {
                first = false;
                return;
            }
            this.typeContext.update((oldTypeContext) => {
                if (typeof oldTypeContext.leave === 'function') {
                    oldTypeContext.leave();
                }
                return createTypeContext(type);
            });
        });
    }

    /**
     * @param {string} newName
     * @returns {boolean}
     */
    isValidNewName(newName) {
        const currentName = get(this.name);

        return newName === currentName || !paletteStorage.hasPaletteName(newName);
    }

    leave() {
        this.subscribeHandler.unsubscribeAll();

        let typeContext = get(this.typeContext);
        if (typeof typeContext.leave === 'function') {
            typeContext.leave();
        }
    }
}

/**
 * @param {ConstructorOptions=} opts
 * @returns {PaletteContext}
 */
export function createRootContext(opts = {}) {
    const rootContext = new PaletteContext(opts);
    setContext(rootContextKey, rootContext);

    return rootContext;
};

/**
 * @returns {PaletteContext}
 */
export function getRootContext() {
    return getContext(rootContextKey);
}

/**
 * @param {PaletteContext} context
 */
export function setRootContext(context) {
    setContext(rootContextKey, context);
}
