/** @typedef {import('svelte').ComponentType} ComponentType */

/**
 * @typedef {object & {
 *     leave?: function(): void,
 *     enter?: function(): void,
 * }} TypeContext
 */

/**
 * @typedef {{
 *     popupContent: ComponentType,
 *     headerCommand?: ComponentType,
 * }} ColorBoxTypeDefinition
 */

export default {};
