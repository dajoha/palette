import PopupContent from './PopupContent.svelte';

/** @typedef {import('../types').ColorBoxTypeDefinition} ColorBoxTypeDefinition */

/** @type {ColorBoxTypeDefinition} */
export default {
    popupContent: PopupContent,
};
