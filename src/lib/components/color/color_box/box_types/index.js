import standalone from './standalone';
import link from './link';

/** @typedef {import('svelte').ComponentType} ComponentType */
/** @typedef {import('./types').ColorBoxTypeDefinition} ColorBoxTypeDefinition */
/** @typedef {import('$lib/types').ColorBoxType} ColorBoxType */

/**
 * @typedef {{
 *     type: ColorBoxType,
 *     command: ComponentType,
 * }} HeaderCommandDefinition
 */

/** @type {Object.<ColorBoxType, ColorBoxTypeDefinition>} */
export const boxTypes = {
    standalone,
    link,
};

 /** @type {HeaderCommandDefinition[]} */
export const headerCommands = Object.entries(boxTypes)
    .filter(([_, boxType]) => boxType.headerCommand)
    .map(([t, boxType]) => ({
        type: /** @type {ColorBoxType} */ (t),
        command: boxType.headerCommand,
    }));
