import { uid } from 'uid';

import { get, writable } from 'svelte/store';

/** @template T @typedef {import('svelte/store').Writable<T>} Writable */
/** @typedef {import('svelte/store').Unsubscriber} Unsubscriber */
/** @typedef {import('$types').ColorBoxContext} ColorBoxContext */
/** @typedef {import('../types').TypeContext} TypeContext */

/**
 * TODO: add real "extends" to real class `TypeContext`.
 * @extends {TypeContext}
 */
export class LinkTypeContext {
    /**
     * @param {ColorBoxContext} rootContext
     * @param {ColorBoxContext} linkedBoxContext
     */
    constructor(rootContext, linkedBoxContext) {
        this.id = uid();
        /** @type {ColorBoxContext} */
        this.linkedBoxContext = linkedBoxContext;
        /** @type {ColorBoxContext} */
        this.rootContext = rootContext;
        /** @type {Unsubscriber[]} */
        this.unsubscribers = [];

        // See `enter()` below, which must called after construction.
    }

    /**
     * @param {Unsubscriber=} unsubscribe
     */
    addUnsubscriber(unsubscribe) {
        if (unsubscribe) {
            this.unsubscribers.push(unsubscribe);
        }
    }

    unsubscribe() {
        for (const unsubscribe of this.unsubscribers) {
            unsubscribe();
        }
        this.unsubscribers = [];
    }

    enter() {
        this.linkedBoxContext.registerReverseLink(this);
        this.addUnsubscriber(this.linkedBoxContext.colorStore.subscribe(color => {
            this.rootContext.colorStore.set(color);
        }));
    }

    leave() {
        this.unsubscribe();
        this.linkedBoxContext.unregisterReverseLink(this);
        const color = get(this.rootContext.colorStore);
        this.rootContext.colorStore.set(writable(get(color).clone()));
    }

    unlink() {
        this.rootContext.setTypeStandalone();
    }
}
