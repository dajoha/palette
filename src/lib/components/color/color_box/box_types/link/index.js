/** @typedef {import('../types').ColorBoxTypeDefinition} ColorBoxTypeDefinition */

import HeaderCommand from './HeaderCommand.svelte';
import PopupContent from './PopupContent.svelte';

/** @type {ColorBoxTypeDefinition} */
export default {
    popupContent: PopupContent,
    headerCommand: HeaderCommand,
};
