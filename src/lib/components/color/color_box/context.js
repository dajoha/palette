import { getContext, setContext } from 'svelte';
import { writable } from 'svelte/store';

import { Color } from '$lib/model/color';

/** @template T @typedef {import('svelte/store').Writable<T>} Writable */
/** @typedef {import('$types').ColorBoxType} ColorBoxType */
/** @typedef {import('./box_types/types').TypeContext} TypeContext */
/** @typedef {import('./box_types/link/type_context').LinkTypeContext} LinkTypeContext */

/**
 * @typedef {{
 *     initialColor?: Color,
 * }} NewContextOptions
 */

export const rootContextKey = Symbol();

export class ColorBoxContext {
    /**
     * @param {NewContextOptions=} opts
     */
    constructor(opts = {}) {
        const initialColor = opts.initialColor ? opts.initialColor.clone() : Color.default();

        /** @type {Writable<Writable<Color>>} */
        this.colorStore = writable(writable(initialColor));
        /** @type {Writable<ColorBoxType>} */
        this.type = writable('standalone');
        /** @type {Writable<TypeContext>} */
        this.typeContext = writable({}); // TODO: replace `{}` by getting type context for `standalone`
        /** @type {Writable<boolean>} */
        this.edit = writable(false);
        /** @type {Writable<boolean>} */
        this.revealed = writable(false);
        /** @type {Object.<string, LinkTypeContext>} */
        this.reverseLinkedBoxContexts = {};
    }

    leave() {
        this.unlinkRegisteredReverseLinks();
    }

    /**
     * @param {ColorBoxType} type
     * @param {TypeContext} typeContext
     */
    setType(type, typeContext) {
        this.type.set(type);
        this.typeContext.update((oldTypeContext) => {
            if (typeof oldTypeContext.leave === 'function') {
                oldTypeContext.leave();
            }
            if (typeof typeContext.enter === 'function') {
                typeContext.enter();
            }
            return typeContext;
        });
    }

    setTypeStandalone() {
        this.setType('standalone', {});
    }

    startEdit() {
        this.edit.set(true);
    }

    stopEdit() {
        this.edit.set(false);
    }

    reveal() {
        this.revealed.set(true);
    }

    unreveal() {
        this.revealed.set(false);
    }

    /**
     * Called when a color box is destroyed: this transforms all reverse-linked colors to standalone
     * colors.
     */
    unlinkRegisteredReverseLinks() {
        for (const reverseLinkedBoxContext of Object.values(this.reverseLinkedBoxContexts)) {
            /** @type {LinkTypeContext} */ (reverseLinkedBoxContext).unlink();
        }
    }

    /**
     * @param {LinkTypeContext} linkContext
     */
    registerReverseLink(linkContext) {
        this.reverseLinkedBoxContexts[linkContext.id] = linkContext;
    }

    /**
     * @param {LinkTypeContext} linkContext
     */
    unregisterReverseLink(linkContext)
    {
        delete this.reverseLinkedBoxContexts[linkContext.id];
    }
}

/**
 * @param {NewContextOptions=} opts
 * @returns {ColorBoxContext}
 */
export function createRootContext(opts = {}) {
    const rootContext = new ColorBoxContext(opts);
    setContext(rootContextKey, rootContext);

    return rootContext;
};

/**
 * @returns {ColorBoxContext}
 */
export function getRootContext() {
    return getContext(rootContextKey);
}
