import { cssVariables } from '$lib/constants';
import { setCssVariable } from '$lib/util/css';

export function addGlobalCssVariables() {
    for (const variable in cssVariables) {
        setCssVariable(variable, `${cssVariables[variable]}`);
    }
}

export default function() {
    addGlobalCssVariables();
}
