/** @typedef {'normal'|'select_box'} AppStateMode */

/** @typedef {function(ColorBoxContext): void} AppStateSelectBoxCallback */

/**
 * @typedef {{
 *     mode: AppStateMode,
 *     selectBoxCallback: null|AppStateSelectBoxCallback
 * }} AppState
 */

/** @typedef {import('$lib/model/color').Color} Color */

/** @typedef {import('$lib/components/color/color_box/ColorBox.svelte').default} ColorBox */

/** @typedef {'standalone'|'link'} ColorBoxType */

/** @typedef {import('$lib/components/color/color_box/context').ColorBoxContext} ColorBoxContext */

/** @typedef {import('$lib/components/color/palette/context').PaletteContext} PaletteContext */

export default {};
