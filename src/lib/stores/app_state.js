import { writable } from 'svelte/store';

/** @typedef {import('$types').AppState} AppState */
/** @typedef {import('$types').AppStateSelectBoxCallback} AppStateSelectBoxCallback */
/** @template T @typedef {import('svelte/store').Writable<T>} Writable */

/**
 * @typedef {{
 *     setModeNormal: function(): void,
 *     setModeSelectBox: function(AppStateSelectBoxCallback): void,
 * }} AppStateStoreExtra
 */

/**
 * @returns {Writable<AppState> & AppStateStoreExtra}
 */
function createAppState() {
    /** @type {AppState} */
    const appState = {
        mode: 'normal',
        selectBoxCallback: null,
    };

    const { subscribe, set, update } = writable(appState);

    return {
        subscribe,
        set,
        update,
        setModeNormal: () => update(state => ({
            ...state,
            ...{ mode: 'normal', selectBoxCallback: null },
        })),
        setModeSelectBox: callback => update(state => ({
            ...state,
            ...{ mode: 'select_box', selectBoxCallback: callback },
        })),
    };
}

/**
 * @type {Writable<AppState> & AppStateStoreExtra}
 */
export const appState = createAppState();
