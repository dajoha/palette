import { get, writable } from 'svelte/store';
import { ObjectStorage } from '$lib/util/object_storage';

/** @typedef {import('$types').PaletteContext} PaletteContext */
/** @template T @typedef {import('svelte/store').Writable<T>} Writable */

class PaletteStorage {
    /** @type {Writable<ObjectStorage<PaletteContext>>} */
    objectStorage = writable(new ObjectStorage());

    /**
     * @param {string} id
     * @param {PaletteContext} object
     */
    add(id, object) {
        this.objectStorage.update(storage => {
            storage.add(id, object);
            return storage;
        });
    }

    /**
     * @param {string} id
     */
    remove(id) {
        this.objectStorage.update(storage => {
            storage.remove(id);
            return storage;
        });
    }

    /**
     * @returns {string[]}
     */
    getPaletteNames() {
        return get(this.objectStorage)
            .all()
            .map(paletteContext => get(paletteContext.name));
    }

    /**
     * @param {string} name
     * @returns {boolean}
     */
    hasPaletteName(name) {
        return this.getPaletteNames().includes(name);
    }
}

export const paletteStorage = new PaletteStorage();
