export const transitionSpeed = 300; // milli-seconds
export const flipDuration = transitionSpeed / 2;
export const colorBoxSize = '30px';

/**
 * Css variables which will be defined on startup.
 * @type {Object.<string, string|number>}
 */
export const cssVariables = {
    '--transition-speed-ms': transitionSpeed,
    '--color-box--size': colorBoxSize,
};
