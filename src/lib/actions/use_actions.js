// Adapted from:
// https://github.com/hperrin/svelte-material-ui/blob/6ed73be2ffd9691182a3a0663d61f9d910c4a8e8/packages/common/src/internal/useActions.ts

/**
 * @template P
 * @typedef {{
 *   update?: (newParams?: P) => void;
 *   destroy?: () => void;
 * } | void} SvelteActionReturnTypeAlt
 */

/**
 * @template P
 * @typedef {(node: HTMLElement, params?: P) => SvelteActionReturnTypeAlt<P>} SvelteHTMLActionType
 */

/**
 * @template P
 * @typedef {SvelteHTMLActionType<P> | [SvelteHTMLActionType<P>, P]} HTMLActionEntry
 */

/**
 * @template P
 * @typedef {HTMLActionEntry<P>[]} HTMLActionArray;
 */

/**
 * @template P
 * @typedef {HTMLActionArray<P>} ActionArray;
 */


/**
 * @template P
 * @param {HTMLElement} node
 * @param {ActionArray<P>} actions
 */
export function useActions(node, actions) {
    /**
     * @template P
     * @type {SvelteActionReturnTypeAlt<P>[]}
     */
    let actionReturns = [];

    if (actions) {
        for (let i = 0; i < actions.length; i++) {
            const actionEntry = actions[i];
            const action = Array.isArray(actionEntry) ? actionEntry[0] : actionEntry;
            if (Array.isArray(actionEntry) && actionEntry.length > 1) {
                actionReturns.push(action(node, actionEntry[1]));
            } else {
                actionReturns.push(action(node));
            }
        }
    }

    return {
        /**
         * @param {ActionArray<P>} actions
         */
        update(actions) {
            if (((actions && actions.length) || 0) != actionReturns.length) {
                throw new Error('You must not change the length of an actions array.');
            }

            if (actions) {
                for (let i = 0; i < actions.length; i++) {
                    const returnEntry = actionReturns[i];
                    if (returnEntry && returnEntry.update) {
                        const actionEntry = actions[i];
                        if (Array.isArray(actionEntry) && actionEntry.length > 1) {
                            returnEntry.update(actionEntry[1]);
                        } else {
                            returnEntry.update();
                        }
                    }
                }
            }
        },

        destroy() {
            for (let i = 0; i < actionReturns.length; i++) {
                const returnEntry = actionReturns[i];
                if (returnEntry && returnEntry.destroy) {
                    returnEntry.destroy();
                }
            }
        },
    };
}
