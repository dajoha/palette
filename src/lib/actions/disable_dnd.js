/**
 * @param {HTMLElement} node
 */
export function disableDnd(node) {
    const onEvent = (/** @type {Event} */ event) => {
        event.stopImmediatePropagation();
    };

    node.addEventListener('mousedown', onEvent, true);
    node.addEventListener('touchstart', onEvent, true);

    return {
        destroy() {
            node.removeEventListener('mousedown', onEvent, true);
            node.removeEventListener('touchstart', onEvent, true);
        }
    };
}
