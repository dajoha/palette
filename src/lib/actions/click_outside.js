/**
 * @param {HTMLElement} node
 * @param {{
 *     exclude?: Node,
 * }} [options]
 */
export function clickOutside(node, options = {}) {
    const handleClick = (/** @type {MouseEvent} */ event) => {
        /** @type {Node|null} */ // @ts-ignore
        const target = event.target;
        if (
            !node.contains(target)
            && !(options.exclude && options.exclude.contains(target))
        ) {
            node.dispatchEvent(new CustomEvent('outclick'));
        }
    };

    document.addEventListener('mousedown', handleClick, true);

    return {
        destroy() {
            document.removeEventListener('mousedown', handleClick, true);
        }
    };
}
